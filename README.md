# This is a repository for kubernentes workshop

## Kubernetess cheatsheet

Applay 

```console
kubectl apply -f kubernetes-dashboard.yaml
```


Run proxy which allows acess kubernetes

```console
kubectl proxy
```

url to dashboard

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/.


Run a Hello World application in your cluster

```console
kubectl apply -f .\services\serviceload-balancer-example.yaml
```


Display information about the Deployment:

```console
kubectl get deployments hello-world
kubectl describe deployments hello-world
```


Display information about your ReplicaSet objects:

```console
kubectl get replicasets
kubectl describe replicasets
```


Create a Service object that exposes the deployment:

```console
kubectl expose deployment hello-world --type=LoadBalancer --name=my-service
```


Display information about the Service:

```console
kubectl get services my-service
```


Display detailed information about the Service:

```console
kubectl describe services my-service
kubectl get pods --output=wide
```


Cleaning up
To delete the Service, enter this command:

```console
kubectl delete services my-service
```

To delete the Deployment, the ReplicaSet, and the Pods that are running the Hello World application, enter this command:

```console
kubectl delete deployment hello-world
```